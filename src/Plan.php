<?php

namespace Laravel\Spark;

use JsonSerializable;
use Illuminate\Support\Arr;

class Plan implements JsonSerializable
{
    /**
     * The plan's Stripe ID.
     */
    public string $id;

    /**
     * The plan's displayable name.
     */
    public string $name;

    /**
     * The plan's price.
     */
    public int $price = 0;

    /**
     * The plan's interval.
     */
    public string $interval = 'monthly';

    /**
     * The number of trial days that come with the plan.
     */
    public int $trialDays = 0;

    /**
     * The plan's features.
     */
    public array $features = [];

    /**
     * The plan's attributes.
     */
    public array $attributes = [];

    /**
     * Indicates if the plan is active.
     */
    public bool $active = true;

    /**
     * The style of the plan.
     */
    public string $type = 'user';

    /**
     * Create a new plan instance.
     */
    public function __construct(string $name, string $id)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * Set the price of the plan.
     */
    public function price(string|int $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Specify that the plan is on a yearly interval.
     */
    public function yearly(): self
    {
        $this->interval = 'yearly';

        return $this;
    }

    /**
     * Specify the number of trial days that come with the plan.
     */
    public function trialDays(int $trialDays): self
    {
        $this->trialDays = $trialDays;

        return $this;
    }

    /**
     * Specify the plan's features.
     */
    public function features(array $features): self
    {
        $this->features = $features;

        return $this;
    }

    /**
     * Get a given attribute from the plan.
     */
    public function attribute(string $key, mixed $default = null): mixed
    {
        return Arr::get($this->attributes, $key, $default);
    }

    /**
     * Set the maximum number of teams that can be owned for this plan.
     */
    public function maxTeams(int $max): self
    {
        return $this->attributes(['teams' => $max]);
    }

    /**
     * Set the maximum number of total collaborators an account may have.
     */
    public function maxCollaborators(int $max): self
    {
        return $this->attributes(['collaborators' => $max]);
    }

    /**
     * Set the maximum number of team members that a team may have.
     */
    public function maxTeamMembers(int $max): self
    {
        return $this->attributes(['teamMembers' => $max]);
    }

    /**
     * Specify the plan's attributes.
     */
    public function attributes(array $attributes): self
    {
        $this->attributes = array_merge($this->attributes, $attributes);

        return $this;
    }

    /**
     * Indicate that the plan should be archived.
     */
    public function archived(): self
    {
        $this->active = false;

        return $this;
    }

    /**
     * Get the array form of the plan for serialization.
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'trialDays' => $this->trialDays,
            'interval' => $this->interval,
            'features' => $this->features,
            'active' => $this->active,
            'attributes' => $this->attributes,
            'type' => $this->type,
        ];
    }

    /**
     * Dynamically access the plan's attributes.
     */
    public function __get(string $key): mixed
    {
        return $this->attribute($key);
    }
}
