<?php

namespace Laravel\Spark\Providers;

use Laravel\Spark\Spark;
use Laravel\Cashier\Cashier;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Your application and company details.
     */
    protected array $details = [];

    /**
     * All the application developer e-mail addresses.
     */
    protected array $developers = [];

    /**
     * The address where customer support e-mails should be sent.
     */
    protected ?string $sendSupportEmailsTo = null;

    /**
     * The available team member roles.
     */
    protected array $roles = [];

    /**
     * Indicates if two-factor authentication should be offered.
     */
    protected bool $usesTwoFactorAuth = false;

    /**
     * Indicates if the application will expose an API.
     */
    protected bool $usesApi = true;

    /**
     * All the abilities that may be assigned to API tokens.
     */
    protected array $tokensCan = [];

    /**
     * The token abilities that should be selected by default in the UI.
     */
    protected array $byDefaultTokensCan = [];

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Spark::details($this->details);

        Spark::sendSupportEmailsTo($this->sendSupportEmailsTo);

        if (count($this->developers) > 0) {
            Spark::developers($this->developers);
        }

        if (count($this->roles) > 0) {
            Spark::useRoles($this->roles);
        }

        if ($this->usesTwoFactorAuth) {
            Spark::useTwoFactorAuth();
        }

        if ($this->usesApi) {
            Spark::useApi();
        }

        Spark::tokensCan($this->tokensCan);

        Spark::byDefaultTokensCan($this->byDefaultTokensCan);

        //$this->booted();
    }

    /**
     * Register any application services.
     */
    public function register(): void
    {
        Cashier::ignoreMigrations();
    }
}
